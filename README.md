# Svelteworld

## About:

Svelteworld is a Svelte component that contains the list of countries in accordance with the ISO 3155-1 standard. This list cointains 249 countries - independent nations, unrecognized countries and plain territories. The motivation behind the creation of this component is general lack of complete lists of countries of the World which complicates the development process. Also, please leave politics aside, because this is made in accordance with the aforementioned standard.

## Installation:

Use the NPM package manager to install Svelteworld. After the installation, import the component:

```js
import Svelteworld from "svelteworld";
```

And use the component like any other component in Svelte:

```html
<Svelteworld/>
```

## Props:

Svelteworld contains a `<div>` that is wrapped around the `<select>` which contains the `<option>` list of 249 countries. Props are variables of the String value (though set as `undefined` by default) meant for styling of the component:


| Props              | Explanation                                                           |
| ------------------ | --------------------------------------------------------------------- |
| divClass           | class of the `<div>`  which is wrapped around the `<select>` element  |  
| divId              | the id of the `<div>` which is wrapped around the `<select>` element  |
| selectClass        | class of the `<select>` element                                       |  
| selectId           | the id of of the `<select>` element                                   |
| optionColor        | text color of the `<option>` element                                  |
| selectBackground   | background color of the `<select>` element                            |
| selectWidth        | width of the `<select>` element                                       |  
| selectHeight       | height of the `<select>` element                                      |
| selectPadding      | padding of the `<select>` element                                     |
| selectMargin       | margin of the `<select>` element                                      |
| selectBorderRadius | border radius of the `<select>` element                               |  
| selectFontSize     | font-size of the `<option>` element                                   |


For example, if you want to set the color of options inside the `<select>`:

```html
<Svelteworld optionColor = 'blue'/>
```

Or, if you want to set the border radius of the `<select>` you should do as follows:

```html
<Svelteworld selectBorderRadius = '10px'/>
```

## Contact:
stefan@cipher.global
